export enum CardType {
  TOP_CARD = 'top',
  NORMAL_CARD = 'normal',
  TOP_CARD_HORIZONTAL = 'horizontal',
}
