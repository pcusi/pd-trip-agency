export const About = () => {
  return (
    <div class="flex flex-col gap-20">
      <div class="flex flex-col gap-6 lg:w-[480px]">
        <h1 class="text-5xl text-black font-bold leading-16">Explora tus viajes a otro nivel</h1>
        <p class="text-base text-gray-400 font-thin font-poppins leading-8">Tu aventura perfecta comienza aquí: encuentra tu destino ideal con nuestra ayuda.</p>
      </div>
    </div>
  );
};
