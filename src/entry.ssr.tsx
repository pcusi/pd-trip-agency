import {
  renderToStream,
  type RenderToStreamOptions,
} from '@builder.io/qwik/server';
import { manifest } from '@qwik-client-manifest';
import { isDev } from '@builder.io/qwik/build';
import Root from './root';

export default function (opts: RenderToStreamOptions) {
  if (isDev) {
    const consoleWarn = console.warn;
    const SUPPRESSED_WARNINGS = [
      'Duplicate implementations of "JSXNode" found',
    ];

    console.warn = function filterWarnings(msg, ...args) {
      if (
        !SUPPRESSED_WARNINGS.some(
          (entry) =>
            msg.includes(entry) || args.some((arg) => arg.includes(entry)),
        )
      )
        consoleWarn(msg, ...args);
    };
  }

  return renderToStream(<Root />, {
    manifest,
    ...opts,
    // Use container attributes to set attributes on the html tag.
    containerAttributes: {
      lang: 'en-us',
      ...opts.containerAttributes,
    },
  });
}
