import {
  HiClockOutline,
  HiCurrencyDollarOutline,
  HiStarOutline,
  HiMapPinOutline,
  HiUserGroupOutline,
  HiCalendarDaysOutline,
} from '@qwikest/icons/heroicons';
import { Header } from "@/shared/header/Header";

export const Hero = () => {
  return (
    <section class="w-full">
      <div class="relative lg:max-w-full lg:h-[880px] lg:max-h-[880px]">
        <div class="bg-black/[.45] w-full h-full absolute z-10 lg:top-0" />
        <img
          src="/img/agency_tingo_maria.jpg"
          alt="Agency Tingo María"
          class="w-full h-full object-cover absolute lg:top-0 lg:z-0"
        />
        <div class="relative w-full flex flex-col justify-between h-full z-20 lg:py-6 lg:container">
          <Header />
          <div class="flex flex-col items-center">
            <div class="flex flex-col gap-10">
              <span class="text-sky-200 text-lg text-center uppercase font-poppins">
                Nuevo Tour
              </span>
              <h1 class="text-white font-bold text-center lg:text-6xl">
                Tingo María
              </h1>
              <div class="flex flex-row items-center gap-10">
                <div class="flex flex-row gap-2 items-center">
                  <HiClockOutline class="text-orange-500 text-2xl" />
                  <p class="text-white text-sm font-poppins">3 días 2 noches</p>
                </div>
                <div class="flex flex-row gap-2 items-center">
                  <HiStarOutline class="text-orange-500 text-2xl" />
                  <p class="text-white text-sm font-poppins">4.64 reseñas</p>
                </div>
                <div class="flex flex-row gap-2 items-center">
                  <HiCurrencyDollarOutline class="text-orange-500 text-2xl" />
                  <p class="text-white text-sm font-poppins">S/ 120</p>
                </div>
              </div>
              <button class="self-start mx-auto bg-orange-500 text-white py-3 px-6 rounded-lg">
                Agendar reserva
              </button>
            </div>
          </div>
          <div class="bg-black/[.75] w-full p-8 rounded-xl">
            <div class="grid grid-cols-3 gap-5">
              <div class="flex flex-row gap-2 items-center relative p-2.5 line-after">
                <HiMapPinOutline class="text-slate-500 text-xl" />
                <p class="text-slate-500 font-poppins">¿A dónde quieres ir?</p>
              </div>
              <div class="flex flex-row gap-2 items-center relative p-2.5 line-after">
                <HiCalendarDaysOutline class="text-slate-500 text-xl" />
                <input
                  type="date"
                  placeholder="Departure day"
                  class="text-slate-500 bg-transparent border-none focus:outline-none focus:border-none font-poppins"
                />
              </div>
              <div class="flex flex-row gap-2 items-center p-2.5">
                <HiUserGroupOutline class="text-slate-500 text-xl" />
                <p class="text-slate-500 font-poppins">
                  ¿Cuántas personas son?
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
