import { HiUserOutline } from '@qwikest/icons/heroicons';

export const Header = () => {
  return (
    <nav class="w-full flex flex-row justify-between">
      <a href="" class="text-2xl text-white font-bold">
        Travel Perú
      </a>
      <ul class="flex-shrink-0 flex-none flex-row gap-4 items-center lg:flex">
        <li>
          <a href="" class="text-white text-sm">
            Alojamientos
          </a>
        </li>
        <li>
          <a href="" class="text-white text-sm">
            Paquetes
          </a>
        </li>
        <li>
          <a href="" class="text-white text-sm">
            Ofertas
          </a>
        </li>
        <li>
          <a href="" class="text-white text-sm">
            Contáctanos
          </a>
        </li>
      </ul>
      <div class="flex flex-row gap-1">
        <HiUserOutline class="text-lg cursor-pointer text-white" />
      </div>
    </nav>
  );
};
