import { component$ } from '@builder.io/qwik';
import type { DocumentHead } from '@builder.io/qwik-city';
import { DocumentHeadTitle } from '@/infrastructure/enum/DocumentHeadTitle';
import { Hero } from '@/shared/hero/Hero';
import { About } from "@/components/index";

export default component$(() => {
  return (
    <>
      <Hero />
      <div class="lg:container my-20">
        <About />
      </div>
    </>
  );
});

export const head: DocumentHead = {
  title: DocumentHeadTitle.INDEX,
  meta: [
    {
      name: 'description',
      content: 'Travel Perú - Hacemos tus viajes increíbles',
    },
    {
      name: 'author',
      content: 'Travel Perú',
    },
  ],
};
